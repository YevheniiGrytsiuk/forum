﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using DAL.Entities;
using BL.Models;

namespace BL.MapperProfile
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Topic, TopicModel>()
                .ReverseMap();
            CreateMap<Forum, ForumModel>()
                .ForMember(dest => dest.ForumTypeName, opt => opt.MapFrom(src => src.ForumType.Name))
                .ReverseMap();
            CreateMap<ForumType, ForumTypeModel>()
                .ForMember(dest => dest.Forums, opt => opt.MapFrom(src => src.Forums))
                .ReverseMap();
        }
    }
}
