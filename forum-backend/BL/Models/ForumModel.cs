﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL.Models
{
    public class ForumModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Header { get; set; }
        [Required]
        public string SubHeader { get; set; }
        [Required]
        public string ForumTypeName { get; set; }
        public ICollection<TopicModel> Topics { get; set; }
    }
}
