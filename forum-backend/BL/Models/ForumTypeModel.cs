﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Models
{
    public class ForumTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ForumModel> Forums { get; set; }
    }
}
