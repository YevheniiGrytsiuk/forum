﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL.Models
{
    public class TopicModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Header { get; set; }
        public int ForumId { get; set; }
    }
}
