﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Services.Abstract
{
    public interface IAutoMapperService
    {
        IMapper Mapper { get; }
    }
}
