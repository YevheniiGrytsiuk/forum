﻿using BL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services.Abstract
{
    public interface IForumService : ICrud<ForumModel>
    {
        Task<IEnumerable<ForumTypeModel>> GetAllForumTypeWithDelaisAsync();
        Task<ForumModel> GetByIdWithDetailsAsync(int id);
    }
}
