﻿using AutoMapper;
using BL.Models;
using DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Services.Abstract
{
    public interface ITopicService : ICrud<TopicModel>
    {

    }
}
