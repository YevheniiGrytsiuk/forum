﻿using AutoMapper;
using BL.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Services.Concrete
{
    public class AutoMapperService : IAutoMapperService
    {
        public static IConfigurationProvider Configuration 
        {
            get 
            {
                return config.Value;
            }
        }
        private readonly static Lazy<IConfigurationProvider> config = new Lazy<IConfigurationProvider>(() =>
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BL.MapperProfile.AutomapperProfile>();
            });
        });
        private readonly Lazy<IMapper> mapper = new Lazy<IMapper>(() =>
        {
            return new Mapper(Configuration);
        });
        
        public IMapper Mapper 
        {
            get 
            {
                return mapper.Value;
            }
        }
    }
}
