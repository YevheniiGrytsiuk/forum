﻿using AutoMapper;
using BL.Models;
using BL.Services.Abstract;
using DAL.Context;
using DAL.Entities;
using DAL.Repositories.Abstract;
using Microsoft.EntityFrameworkCore.InMemory.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services.Concrete
{
    public class ForumService : IForumService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ForumService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task AddAsync(ForumModel model)
        {
            Forum modelToAdd = _mapper.Map<Forum>(model);

            await _unitOfWork.ForumRepository.AddAsync(modelToAdd);
            await _unitOfWork.Commit();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _unitOfWork.ForumRepository.DeleteByIdAsync(id);
        }

        public IEnumerable<ForumModel> GetAll()
        {
            var forums = _unitOfWork.ForumRepository.GetAllWithDetails();

            var forumsModel = _mapper
                .Map<IQueryable<Forum>, IEnumerable<ForumModel>>(forums);

            return forumsModel;
        }
        // TODO: config mapper
        public async Task<ForumModel> GetByIdAsync(int id)
        {
            var forum = await _unitOfWork.ForumRepository.GetByIdAsync(id);

           

            var forumModel = _mapper.Map<ForumModel>(forum);

            

            return forumModel;
        }
        public async Task<ForumModel> GetByIdWithDetailsAsync(int id) 
        {
            var forum = await _unitOfWork.ForumRepository.GetByIdWithDetails(id);
            
            if (forum == null)
                throw new Exception();

            var forumsTopics = await Task.Run(() => _unitOfWork.TopicRepository.GetByCondition(topic => topic.ForumId == forum.Id).AsEnumerable());
            
            var mappedStuff = _mapper.Map<IEnumerable<TopicModel>>(forumsTopics);

            

            var forumModel = _mapper.Map<ForumModel>(forum);
            forumModel.Topics = new List<TopicModel>(mappedStuff);
            return forumModel;
        }

        public async Task UpdateAsync(ForumModel model)
        {
            var modelToUpdate = _mapper.Map<Forum>(model);

            await Task.Run(() => _unitOfWork.ForumRepository.Update(modelToUpdate));
        }
        public async Task<IEnumerable<ForumTypeModel>> GetAllForumTypeWithDelaisAsync() 
        {
            var forumTypesWithForums = await Task.Run(() => _unitOfWork.ForumTypeRepository.GetAllWithDetails());
            //_mapper.Map<ForumTypeModel>(forumTypesWithForums);
            var mappedForumTypesWithForums = TempMapp(forumTypesWithForums);
            return mappedForumTypesWithForums;
        }
        private ICollection<ForumTypeModel> TempMapp(IEnumerable<ForumType> entities) 
        {
            ICollection<ForumTypeModel> model = new List<ForumTypeModel>();

            foreach (var entity in entities)
            {
                var shit = new ForumTypeModel
                {
                    Id = entity.Id,
                    Name = entity.Name,

                    
                };
                shit.Forums = _mapper.Map<ICollection<ForumModel>>(entity.Forums);

                model.Add
                    (
                        shit                        
                    );
            }
            return model;
        }
    }
}
