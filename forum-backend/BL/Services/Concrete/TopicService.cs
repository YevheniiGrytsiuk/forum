﻿using AutoMapper;
using BL.Services.Abstract;
using DAL.Entities;
using BL.Models;
using DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services.Concrete
{
    public class TopicService : ITopicService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TopicService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(TopicModel model)
        {
            Topic modelToAdd = _mapper.Map<Topic>(model);

            await _unitOfWork.TopicRepository.AddAsync(modelToAdd);
            await _unitOfWork.Commit();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _unitOfWork.TopicRepository.DeleteByIdAsync(id);
        }

        public IEnumerable<TopicModel> GetAll()
        {
            var topics = _unitOfWork.TopicRepository.GetAllWithDetails();

            var topicsModel = _mapper
                .Map<IQueryable<Topic>, IEnumerable<TopicModel>>(topics);

            return topicsModel;
        }

        public async Task<TopicModel> GetByIdAsync(int id)
        {
            var topic = await _unitOfWork.TopicRepository.GetByIdWithDetails(id);

            var topicModel = _mapper.Map<TopicModel>(topic);

            return topicModel;
        }

        public async Task UpdateAsync(TopicModel model)
        {
            var modelToUpdate = _mapper.Map<Topic>(model);

            await Task.Run(() => _unitOfWork.TopicRepository.Update(modelToUpdate));
        }
    }
}
