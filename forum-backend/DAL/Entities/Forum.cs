using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Forum
    {
        public Forum()
        {
            Topics = new HashSet<Topic>();
        }
        [Key]
        [StringLength(38)]
        public int Id { get; set; }
        [Required]
        public string Header { get; set; }
        [Required]
        public string SubHeader { get; set; }
        public int ForumTypeId { get; set; }

        public virtual ForumType ForumType { get; set; }
        public virtual ICollection<Topic> Topics { get; set; }
    }
}
