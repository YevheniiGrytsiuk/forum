using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class ForumType 
    {
        public ForumType()
        {
            Forums = new HashSet<Forum>();
        }
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual ICollection<Forum> Forums { get; set; }
    }

}