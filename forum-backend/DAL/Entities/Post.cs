﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Entities
{
    public class Post
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Header { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public int TopicId { get; set; }
        public int AuthorId { get; set; }
        public int? ParentPostId { get; set; }
        public virtual Topic Topic { get; set; } 

        //public int MyProperty { get; set; }
    }
}
