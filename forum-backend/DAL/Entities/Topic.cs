﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Entities
{
    public class Topic
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Header { get; set; }
        public int ForumId { get; set; }
        public virtual Forum Forum { get; set; }
    }
}
