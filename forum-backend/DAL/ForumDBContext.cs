using Microsoft.EntityFrameworkCore;
using DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace DAL.Context
{
    public class ForumDBContext : IdentityDbContext<ApplicationUser>
    {
        public ForumDBContext() { }
        public ForumDBContext(DbContextOptions<ForumDBContext> options) : base(options)
        {
            // TODO: Uncomment when need to add migration
            //this.Database.EnsureDeleted();
            this.Database.EnsureCreated();
            
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer();
        }
        public DbSet<Forum> Forums { get; set; }
        public DbSet<ForumType> ForumTypes { get; set; }
        public DbSet<Topic> Topic { get; set; }
        public DbSet<ApplicationRole> ApplicationRoles { get; set;  }
        public DbSet<ClientProfile> ClientProfiles { get; set; }
    }
}