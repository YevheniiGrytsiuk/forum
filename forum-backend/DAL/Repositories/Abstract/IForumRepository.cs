﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Abstract
{
    public interface IForumRepository : IRepository<Forum>
    {
        IQueryable<Forum> GetAllWithDetails();
        Task<Forum> GetByIdWithDetails(int id);
    }
}
