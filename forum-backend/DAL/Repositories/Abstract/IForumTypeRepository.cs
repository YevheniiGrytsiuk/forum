﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Entities;

namespace DAL.Repositories.Abstract
{
    public interface IForumTypeRepository : IRepository<ForumType>
    {
        IQueryable<ForumType> GetAllWithDetails();
    }
}
