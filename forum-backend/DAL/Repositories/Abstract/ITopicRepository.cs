﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Abstract
{
    public interface ITopicRepository : IRepository<Topic>
    {
        IQueryable<Topic> GetAllWithDetails();
        Task<Topic> GetByIdWithDetails(int id);
    }
}
