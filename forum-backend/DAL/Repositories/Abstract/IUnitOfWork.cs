using DAL.Entities;
using System.Threading.Tasks;

namespace DAL.Repositories.Abstract 
{
    public interface IUnitOfWork 
    {
        IForumRepository ForumRepository { get; set; }
        ITopicRepository TopicRepository { get; set; }
        IForumTypeRepository ForumTypeRepository { get; set; }
        
        Task<int> Commit();
        void Rollback();
    }

}