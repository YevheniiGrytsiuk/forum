﻿using DAL.Entities;
using DAL.Context;
using DAL.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Concrete
{
    public class ForumRepository : Repository<Forum>, IForumRepository
    {
        public ForumRepository(ForumDBContext context) : base(context) { }

        public IQueryable<Forum> GetAllWithDetails()
        {
            return Entity
                .Include(forum => forum.ForumType);
        }

        public Task<Forum> GetByIdWithDetails(int id)
        {
            return Entity
                .Include(forum => forum.ForumType)
                .FirstOrDefaultAsync(forum => forum.Id == id);
        }
    }
}
