﻿using DAL.Context;
using DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories.Concrete
{
    public class ForumTypeRepository : Repository<ForumType>, IForumTypeRepository
    {
        public ForumTypeRepository(ForumDBContext context): base(context) { }

        public IQueryable<ForumType> GetAllWithDetails()
        {
            return Entity
                .Include(forumType => forumType.Forums);
        }
    }
}
