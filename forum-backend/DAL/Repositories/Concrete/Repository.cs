using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
// using Core.Models;
using DAL.Repositories.Abstract;
using DAL.Context;

namespace DAL.Repositories.Concrete
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly ForumDBContext _forumContext;
        protected DbSet<TEntity> Entity { get; set; }

        public Repository(ForumDBContext forumContext)
        {
            _forumContext = forumContext;
            Entity = forumContext.Set<TEntity>();
        }
        public virtual IQueryable<TEntity> GetAll()
        {
            return Entity.AsNoTracking();
        }
        public virtual IQueryable<TEntity> GetByCondition(Expression<Func<TEntity, bool>> expression)
        {
            return Entity.Where(expression).AsNoTracking();
        }
        public virtual async Task<TEntity> GetByIdAsync(int id)
        {
           return await Entity.FindAsync(id);
        }
        public virtual async Task AddAsync(TEntity entity)
        {
            if (entity == null) 
                throw new ArgumentNullException("entity");

            await Entity.AddAsync(entity);
            //context.SaveChanges();
        }
        public virtual void Update(TEntity entity)
        {
            if (entity == null) 
                throw new ArgumentNullException("entity");
            //context.SaveChanges();
        }
        public virtual async Task DeleteByIdAsync(int id)
        {
            TEntity entityToDelete = await Entity.FindAsync(id);

            await Task.Run(() => Delete(entityToDelete));
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if(_forumContext.Entry(entityToDelete).State == EntityState.Detached) 
            {
                Entity.Attach(entityToDelete);
            }
            Entity.Remove(entityToDelete);
        }
        // public Task<T> FirstOrDefault(Expression<Func<T, bool>> predicate)
        //     => context.Set<T>().FirstOrDefaultAsync(predicate);
    }
}