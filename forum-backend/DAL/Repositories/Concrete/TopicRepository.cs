﻿using DAL.Entities;
using DAL.Context;
using DAL.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Concrete
{
    public class TopicRepository : Repository<Topic>, ITopicRepository
    {
        public TopicRepository(ForumDBContext context): base(context) { }

        public IQueryable<Topic> GetAllWithDetails()
        {
            return Entity
                .Include(topic => topic.Forum);
        }

        public Task<Topic> GetByIdWithDetails(int id)
        {
            return Entity
                .Include(topic => topic.Forum)
                .FirstOrDefaultAsync(topic => topic.Id == id);
        }
    }
}
