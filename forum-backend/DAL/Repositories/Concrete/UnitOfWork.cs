using DAL.Entities;
using DAL.Repositories.Abstract;
using System.Threading.Tasks;
using DAL.Context;

namespace DAL.Repositories.Concrete 
{
    public class UnitOfWork : IUnitOfWork 
    {
        private readonly ForumDBContext _databaseContext;
        private IForumRepository _forumRepository;
        private ITopicRepository _topicRepository;
        private IForumTypeRepository _forumTypeRepository;
        public UnitOfWork(ForumDBContext databaseContext) 
        {
            _databaseContext = databaseContext;
        }

        public IForumRepository ForumRepository 
        { 
            get 
            {
                return _forumRepository ??= new ForumRepository(_databaseContext);
            }
            set 
            {
                this._forumRepository = value;
            } 
        }
        public ITopicRepository TopicRepository
        {
            get
            {
                return _topicRepository ??= new TopicRepository(_databaseContext);
            }
            set
            {
                this._topicRepository = value;
            }
        }
        public IForumTypeRepository ForumTypeRepository
        {
            get
            {
                return _forumTypeRepository ??= new ForumTypeRepository(_databaseContext);
            }
            set
            {
                this._forumTypeRepository = value;
            }
        }

        public async Task<int> Commit() 
        {
            return await _databaseContext.SaveChangesAsync();
        }
        public void Rollback()
        {
            _databaseContext.Dispose();
        }
    }
}