﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System;
using DAL.Context;
using DAL.Repositories.Abstract;
using DAL.Repositories.Concrete;
using BL.Services.Abstract;
using BL.Services.Concrete;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;

namespace Root
{
    public class CompositionRoot
    {
        public static void InjectDependencies(IServiceCollection services) 
        {
            //???
            services.AddScoped<IAutoMapperService, AutoMapperService>();
            services.AddAutoMapper(typeof(BL.MapperProfile.AutomapperProfile));

            services.AddDbContext<ForumDBContext>(opts => 
                opts.UseSqlServer("Data Source=.;Initial Catalog=ForumDB;Trusted_Connection=True;"));
            //services.AddScoped<ForumDBContext>();
            
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IForumRepository, ForumRepository>();
            services.AddScoped<ITopicRepository, TopicRepository>();

            services.AddScoped<ITopicService, TopicService>();
            services.AddScoped<IForumService, ForumService>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddIdentity<ApplicationUser, ApplicationRole>(opts => opts.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ForumDBContext>();

            //var builder = services.AddIdentityCore<ApplicationUser>();
            //var identityBuilder = new IdentityBuilder(builder.UserType,
            //                                  builder.Services);

            ////identityBuilder.AddEntityFrameworkStores<ForumDBContext>();
            //identityBuilder.AddSignInManager<SignInManager<ApplicationUser>>();
        }
    }
}
