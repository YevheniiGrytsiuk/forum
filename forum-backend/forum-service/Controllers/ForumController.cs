﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.Models;
using BL.Services.Abstract;
using Microsoft.AspNetCore.Mvc;

namespace forum_service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ForumController : ControllerBase
    {
        private readonly IForumService _forumService;
        public ForumController(IForumService forumService)
        {
            _forumService = forumService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ForumTypeModel>>> GetAll() 
        {
            try
            {
                var forumTypesWithForums = await _forumService.GetAllForumTypeWithDelaisAsync();

                return Ok(forumTypesWithForums);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong");
            }
        }
        [Route("viewforum/{id}")]
        [HttpGet("{id}")]
        //[HttpGet]
        public async Task<ActionResult<ForumModel>> GetById([FromQuery]int id)
        {
            try
            {
                var forum = await _forumService.GetByIdWithDetailsAsync(id);

                if (forum == null)
                    return NotFound();

                return Ok(forum);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}